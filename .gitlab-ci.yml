include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

default:
  image: registry.gitlab.com/centos/kmods/ci-image:latest
  before_script:
    - git config --global user.email 'sig-kmods@centosproject.org'
    - git config --global user.name 'Kmods SIG'

variables:
  GIT_STRATEGY: none
  EL: 9
  REPO: packages-main
  MOCK: almalinux-9

stages:
  - sync
  - build

mock:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
      when: never
    - when: always
  parallel:
    matrix:
      - ARCH: [x86_64]
  stage: build
  script:
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - 'git diff $(printf "" | git hash-object -t tree --stdin) HEAD src licenses > distro/source-git.patch'
    - mock -r $MOCK-$ARCH --define "dist .el$EL" --buildsrpm --spec distro/*.spec --source distro --resultdir .
    - rm -f distro/source-git.patch
    - mock -r $MOCK-$ARCH --define "dist .el$EL" --resultdir . *.src.rpm
  artifacts:
    name: "$CI_PROJECT_NAME-mock-$ARCH"
    untracked: true
    when: always

cbs:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
  stage: build
  script:
    - cbs version
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - 'git diff $(printf "" | git hash-object -t tree --stdin) HEAD src licenses > distro/source-git.patch'
    - rpmbuild --define "dist .el$EL" --define "_srcrpmdir $PWD" --define "_sourcedir $PWD/distro" -bs distro/*.spec
    - NVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' distro/*.spec)
    - 'cbs buildinfo $NVR 2>/dev/null | grep >/dev/null "State: COMPLETE" || cbs --cert=$CBS build --wait --scratch kmods$EL-$REPO-el$EL *.src.rpm'
    - KERNEL=$(sed -rn "s/%global\s*kernel\s*(.*)$/\1/p" distro/*.spec)
    - |
      if git clone --quiet --branch c$EL/$KERNEL --single-branch $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/" | sed 's@/src/@/rpms/@g') dist-git 2>/dev/null
      then
        DNVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' dist-git/*.spec)
        rpmdev-vercmp "$NVR" "$DNVR" || RETVAL=$?
        if ! [[ $RETVAL == 11 ]]
        then
          echo "Version to be deployed is not newer than version in dist-git."
          exit 0
        fi
      else
        git init -b c$EL/$KERNEL dist-git
        git -C dist-git remote add origin $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/" | sed 's@/src/@/rpms/@g')
      fi
    - |
      if ! [ -e distro/README.md ]
      then
        echo "This is an auto-generated dist-git repository. See $CI_PROJECT_URL for source-git repository." > distro/README.md
      fi
    - |
      if ! [ -e distro/sources ]
      then
        touch distro/sources
      fi
    - \cp -rfT distro dist-git
    - cd dist-git
    - git add .
    - git commit -m "kABI tracking kmod package (kernel >= $KERNEL)"
    - git push origin c$EL/$KERNEL

kabi:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/ && $CI_COMMIT_REF_NAME =~ /c.*\/main/
  stage: sync
  script:
    - |
      if ! git clone --quiet --branch c$EL/upstream --single-branch $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/") .
      then
        git init -b c$EL/upstream
        git remote add origin $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/")
      fi
    - LATEST=$(git tag -l | sed -rn "s/^c$EL\/upstream\/(.*)\$/\1/p" | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | tail -1)
    - echo $LATEST
    - |
      readarray -t NEW < <(for p in $(seq 1 $(curl --header "PRIVATE-TOKEN: ${GITLAB}" -L -f -s --head "${CI_API_V4_URL}/projects/centos%2Fkmods%2Fkernel/repository/tags?per_page=100&page=1" | sed -rn 's/x-total-pages:\s*([0-9]+)\s*$/\1/p')); do curl --header "PRIVATE-TOKEN: ${GITLAB}" -L -f -s "${CI_API_V4_URL}/projects/centos%2Fkmods%2Fkernel/repository/tags?per_page=100&page=$p" | jq -r ".[] | .name | select(. | contains(\"c$EL/\"))"; done | sed "s/^c$EL\///" | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | tac | sed "/${LATEST:-^\$}/Q" | tac)
    - |
      for KERNEL in "${NEW[@]}"
      do
        rm -rf src
        for SRCPATH in drivers/net/ethernet
        do
          SRCS=('jme.*')
          TRANSFORMS=("flags=r;s@.*/src/${SRCPATH}@src@g")
          while :
          do
            curl --header "PRIVATE-TOKEN: $GITLAB" -L -f -s "${CI_API_V4_URL}/projects/centos%2Fkmods%2Fkernel/repository/archive?sha=c$EL/$KERNEL&path=src/$SRCPATH" --output - | tar -x -z -f - ${TRANSFORMS[@]/#/--transform } "${SRCS[@]/#/*\/src\/$SRCPATH\/}" 2>/dev/null || [[ ${PIPESTATUS[0]} -eq 0 ]] && break
            sleep 5
          done
        done
        if [[ -n ${FILTER:-} ]]
        then
          "${FILTER[@]}"
        fi
        git add src
        if ! git diff-index --quiet HEAD
        then
          git commit --quiet -m "Import kernel-$KERNEL"
          git push origin HEAD:refs/heads/c$EL/upstream
        fi
        git tag c$EL/upstream/$KERNEL
        git push origin c$EL/upstream/$KERNEL
      done
    - cbs version
    - git fetch --quiet origin $CI_COMMIT_REF_NAME:$CI_COMMIT_REF_NAME
    - git checkout --quiet $CI_COMMIT_REF_NAME
    - NAME=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}\n' distro/*.spec)
    - NVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' distro/*.spec)
    - 'cbs buildinfo $NVR 2>/dev/null | grep >/dev/null "State: COMPLETE" || exit 1'
    - KERNEL=$(sed -rn "s/%global\s*kernel\s*(.*)$/\1/p" distro/*.spec)
    - CKERNEL=$KERNEL
    - git clone --quiet --branch c$EL --single-branch https://gitlab.com/CentOS/kmods/kabi
    - |
      for KVERSION in $(git -C kabi tag -l | sed -rn "s/^c$EL\/(.*)\$/\1/p" | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | sed -e "0,/$KERNEL/d")
      do
        for ARCH in $(grep "ExclusiveArch" distro/*.spec | awk -F ":" '{print $2}')
        do
            RPM="https://cbs.centos.org/kojifiles/packages/$(rpmspec -q --define "dist .el$EL" --queryformat="%{name}/%{version}/%{release}/$ARCH/%{name}-%{version}-%{release}.$ARCH.rpm\n" distro/*.spec)"
            curl -L -f -s -I -o /dev/null $RPM
            while read -r SVERSION SYMBOL
            do
              if ! git -C kabi show c$EL/$KVERSION:Module.symvers.$ARCH | grep "^$SVERSION\s*$SYMBOL" >/dev/null
              then
                [[ ${PIPESTATUS[0]} -eq 0 ]]
                KERNEL=$KVERSION
                break 3
              fi
            done < <(curl -L -f -s -N $RPM | rpm -qp --requires - | sed -rn 's@^kernel\((.*)\) = (0x[0-9a-f]*)$@\2\t\1@p' | sort -k2)
        done
        if [[ $(echo $KERNEL | sed -rn 's/([0-9\.]+-[0-9]+).*/\1/p') != $(echo $KVERSION | sed -rn 's/([0-9\.]+-[0-9]+).*/\1/p') ]]
        then
          KERNEL=$KVERSION
          break
        fi
      done
      if [[ "$CKERNEL" != "$KERNEL" ]]
      then
        for SVERSION in $(git tag -l | sed -rn "s/^c$EL\/upstream\/(.*)\$/\1/p" | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | tac | sed "/$CKERNEL/Q" | tac | sed "/$KERNEL/Q")
        do
          if [[ $(git rev-list -1 c$EL/upstream/$SVERSION) != $(git rev-list -1 c$EL/upstream/$CKERNEL) ]]
          then
            for KVERSION in $( (echo "$SVERSION" && (git -C kabi tag -l | sed -rn "s/^c$EL\/(.*)\$/\1/p")) | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | sed -e "0,/$SVERSION/d" | sed "/$KERNEL/Q")
            do
                KERNEL=$KVERSION
                break 2
            done
          fi
        done
      fi
      if [[ "$CKERNEL" == "$KERNEL" ]]
      then
        for SVERSION in $( (echo "$KERNEL" && (git tag -l | sed -rn "s/^c$EL\/upstream\/(.*)\$/\1/p")) | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | tac | sed "/$KERNEL/Q" | tac )
        do
          if [[ $(git rev-list -1 c$EL/upstream/$SVERSION) != $(git rev-list -1 c$EL/upstream/$KERNEL) ]]
          then
            for KVERSION in $( (echo "$SVERSION" && (git -C kabi tag -l | sed -rn "s/^c$EL\/(.*)\$/\1/p")) | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | sed -e "0,/$SVERSION/d" | sed "/$KERNEL/Q" )
            do
                KERNEL=$KVERSION
                break 2
            done
          fi
        done
      fi
      if [[ "$CKERNEL" != "$KERNEL" ]]
      then
        git push -o ci.skip origin HEAD:refs/heads/c$EL/$CKERNEL
        if ! git rebase c$EL/upstream/$KERNEL
        then
          echo "Unable to automatically rebase c$EL/main on c$EL/upstream/$KERNEL"
          echo "Manual rebase of c$EL/main for $KERNEL required. Guidance:"
          echo "git checkout c$EL/main"
          echo "git rebase c$EL/upstream/$KERNEL"
          echo "sed -i \"s/%global\s*kernel\s*.*/%global kernel $KERNEL/\" distro/*.spec"
          echo "sed -i \"s/%global\s*baserelease\s*.*/%global baserelease 0/\" distro/*.spec"
          echo "sed -i '/^%changelog$/q' distro/*.spec"
          echo "rpmdev-bumpspec -c \"kABI tracking kmod package (kernel >= $KERNEL)\" -D distro/*.spec"
          echo "sed -i -e :a -e '/^\n*\$/{\$d;N;};/\n\$/ba' distro/*.spec"
          echo "git add distro/*.spec"
          echo "git commit -m \"kABI tracking kmod package (kernel >= $KERNEL)\""
          echo "git push -f origin c$EL/main"
          exit 1
        fi
        sed -i "s/%global\s*kernel\s*.*/%global kernel $KERNEL/" distro/*.spec
        sed -i "s/%global\s*baserelease\s*.*/%global baserelease 0/" distro/*.spec
        sed -i '/^%changelog$/q' distro/*.spec
        rpmdev-bumpspec -c "kABI tracking kmod package (kernel >= $KERNEL)" -D distro/*.spec
        sed -i -e :a -e '/^\n*$/{$d;N;};/\n$/ba' distro/*.spec
        git add distro/*.spec
        git commit -m "kABI tracking kmod package (kernel >= $KERNEL)"
        git push -f origin HEAD:refs/heads/c$EL/main
      fi
